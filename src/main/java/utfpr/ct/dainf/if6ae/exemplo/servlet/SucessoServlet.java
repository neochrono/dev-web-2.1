package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Sucesso Servlet", urlPatterns = {"/sucesso"})
public class SucessoServlet extends HttpServlet {

	private static final long serialVersionUID = -215975228255504697L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		String perfil = request.getParameter("perfil");
		switch (perfil) {
		case "1":
			perfil = "Cliente";
			break;
		case "2":
			perfil = "Gerente";
			break;
		case "3":
			perfil = "Administrador";
			break;
		default:
			break;
		}
		
		
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
                    + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Modelo de Servlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.printf("<h1>Acesso a %s %s permitido</h1>\n", perfil, request.getParameter("login"));
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Exemplo de servlet simples";
    }
}
